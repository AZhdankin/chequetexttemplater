package komandor.texttemplater;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Command {
    private final String result;

    public Command(String result) {
        this.result = result;
    }
    
    @Override
    public String toString() {
        return "COMMAND: " + result;
    }
}
