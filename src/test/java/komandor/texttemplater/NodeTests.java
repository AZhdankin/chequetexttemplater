package komandor.texttemplater;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jesus
 */
public class NodeTests {
    
    public NodeTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
/*
    @Test
    public void tableTest() {
        String str1left, str1right, str2left, str2right;
        int paperWidth = 45;
        
        Table t = new Table(
            new Head(
                Arrays.asList(
                    new Column("left", true, true, 0),
                    new Column("right", false, false, 0)
                )
            ), 
            new Body(
                Arrays.asList(
                    new Cell(1, 1, str1left  = "Строка1слева"),
                    new Cell(1, 2, str1right = "Строка1справа"),
                    new Cell(2, 1, str2left  = "Строка2слева"),
                    new Cell(2, 2, str2right = "Строка2справа")
                )
            )
        );
        
        Render<Table> render = new TableRender();
        render.setPaperWidth(paperWidth);
        
        List<String> result = render.render(t);
        assertTrue("Неверный размер рендеринга таблицы", result.size() == 2);
        
        String str1 = str1left + StringUtils.repeat(" ", paperWidth - str1left.length() - str1right.length()) + str1right;
        assertTrue("Неверная строка 1 рендеринга таблицы", result.get(0).equals(str1));
        
        String str2 = str2left + StringUtils.repeat(" ", paperWidth - str2left.length() - str2right.length()) + str2right;
        assertTrue("Неверная строка 2 рендеринга таблицы", result.get(1).equals(str2));
    }*/
}
