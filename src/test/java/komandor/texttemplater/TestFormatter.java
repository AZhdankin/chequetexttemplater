package komandor.texttemplater;

import java.util.List;
import komandor.texttemplater.tags.fiscal.FiscalBold;
import komandor.texttemplater.tags.fiscal.FiscalCloseDocument;
import komandor.texttemplater.tags.fiscal.FiscalCutPaper;
import komandor.texttemplater.tags.fiscal.FiscalDiscount;
import komandor.texttemplater.tags.fiscal.FiscalFeedPaper;
import komandor.texttemplater.tags.fiscal.FiscalOpenCashbox;
import komandor.texttemplater.tags.fiscal.FiscalOpenDocument;
import komandor.texttemplater.tags.fiscal.FiscalPay;
import komandor.texttemplater.tags.fiscal.FiscalRepeatLastDocument;
import komandor.texttemplater.tags.fiscal.FiscalReturn;
import komandor.texttemplater.tags.fiscal.FiscalSale;
import komandor.texttemplater.tags.fiscal.FiscalShiftReport;
import komandor.texttemplater.tags.fiscal.FiscalSubTotal;
import komandor.texttemplater.tags.fiscal.FiscalXReport;
import komandor.texttemplater.tags.fiscal.FiscalZReport;

/**
 * 
 * @author Andrey V Zhdankin
 */
public class TestFormatter extends Formatter<Command> {

    public TestFormatter(Logger logger) {
        super(logger);
    }
    
    @Override
    public void transform(List<Command> result, FiscalPay fiscalPay){
        result.add(new Command(
            "PAY: cash: " + fiscalPay.getCash() + 
            ", cashless: " + fiscalPay.getCashless() + 
            ", kopilka: " + fiscalPay.getKopilka()
        ));
    }
    
    @Override
    public void transform(List<Command> result, FiscalSale paySale){
        result.add(new Command(
            "SALE: text: " + paySale.getText() + 
            ", amount: " + paySale.getPrice() + 
            ", count: " + paySale.getCount()
        ));
    }
    
    @Override
    public void transform(List<Command> result, FiscalReturn fiscalReturn){
        result.add(new Command(
            "RETURN: text: " + fiscalReturn.getText() + 
            ", amount: " + fiscalReturn.getPrice() + 
            ", count: " + fiscalReturn.getCount()
        ));
    }
    
    @Override
    public void transform(List<Command> result, FiscalOpenDocument fiscalOpenDocument){
        result.add(new Command("OPEN DOCUMENT"));
    }
    
    @Override
    public void transform(List<Command> result, FiscalCloseDocument fiscalCloseDocument){
        result.add(new Command("CLOSE DOCUMENT"));
    }
    
    @Override
    public void transform(List<Command> result, FiscalBold fiscalBold){
        result.add(new Command("SET BOLD: " + fiscalBold.isBold()));
    }
    
    @Override
    public void transform(List<Command> result, String string){
        result.add(new Command("STRING: " + string));
    }

    @Override
    public void transform(List<Command> result, FiscalOpenCashbox openCashbox) {
        result.add(new Command("OPENCASHBOX"));
    }

    @Override
    public void transform(List<Command> result, FiscalFeedPaper feedPaper) {
        result.add(new Command("FEED PAPER: " + feedPaper.getRows()));
    }

    @Override
    public void transform(List<Command> result, FiscalCutPaper cutPaper) {
        result.add(new Command("CUT PAPER: " + cutPaper.isFull()));
    }

    @Override
    public void transform(List<Command> result, FiscalXReport xReport) {
        result.add(new Command("XREPORT: " + xReport.getUserName()));
    }

    @Override
    public void transform(List<Command> result, FiscalZReport zReport) {
        result.add(new Command("ZREPORT: " + zReport.getUserName()));
    }

    @Override
    public void transform(List<Command> result, FiscalRepeatLastDocument repeatLastDocument) {
        result.add(new Command("PRINT LAST DOCUMENT"));
    }

    @Override
    public void transform(List<Command> result, FiscalShiftReport shiftReport) {
        result.add(new Command("SHIFT REPORT: " + shiftReport.getShiftNumber()));
    }

    @Override
    public void transform(List<Command> result, FiscalDiscount discount) {
        result.add(new Command("DISCOUNT: " + discount.getName() + ", AMOUNT: " + discount.getAmount() + ", PERCENT: " + discount.isPercent()));
    }

    @Override
    public void transform(List<Command> result, FiscalSubTotal subTotal) {
        result.add(new Command("SUBTOTAL: " + subTotal.getName()));
    }
}
