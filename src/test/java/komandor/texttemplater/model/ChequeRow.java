package komandor.texttemplater.model;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Andrey V Zhdankin
 */
public class ChequeRow {
    private Article article;
    private BigDecimal count;

    public ChequeRow(Article article, BigDecimal count) {
        this.article = article;
        this.count = count;
    }
    
    public void inc(){
        count = count.add(BigDecimal.ONE);
    }
    
    public void dec(){
        if(!count.equals(BigDecimal.ONE))
            count = count.subtract(BigDecimal.ONE);    
    }

    @XmlElement
    public Article getArticle() {
        return article;
    }

    @XmlAttribute(name="count")
    public BigDecimal getCount() {
        return count;
    }
    
    @XmlAttribute(name="total")
    public BigDecimal getTotal(){
        return article.getPrice().multiply(count).setScale(2, BigDecimal.ROUND_DOWN);
    }
}
