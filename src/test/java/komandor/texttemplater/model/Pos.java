package komandor.texttemplater.model;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Pos {
    private long id;
    private String shop;
    private String pointId;

    public Pos(long id, String shop, String pointId) {
        this.id = id;
        this.shop = shop;
        this.pointId = pointId;
    }

    @XmlAttribute(name="id")
    public long getId() {
        return id;
    }

    @XmlAttribute(name="pointId")
    public String getPointId() {
        return pointId;
    }

    @XmlAttribute(name="shop")
    public String getShop() {
        return shop;
    }
}
