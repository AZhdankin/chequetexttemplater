package komandor.texttemplater.model;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Article {
    private String name;
    private BigDecimal price;

    public Article(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    @XmlAttribute(name="name")
    public String getName() {
        return name;
    }

    @XmlAttribute(name="price")
    public BigDecimal getPrice() {
        return price.setScale(2);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}