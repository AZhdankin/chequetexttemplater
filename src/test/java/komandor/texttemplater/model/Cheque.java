package komandor.texttemplater.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Cheque {
    private long id;
    private Date opened;
    private Date closed;
    private List<ChequeRow> rows = new ArrayList();

    public Cheque(long id) {
        this.id = id;
        opened = new Date();
    }
    
    public void addRow(ChequeRow row){
        rows.add(row);
    }

    public void close() {
        closed = new Date();
    }

    @XmlAttribute(name = "id")
    public long getId() {
        return id;
    }

    @XmlAttribute(name = "closed")
    public Date getClosed() {
        return closed;
    }

    @XmlAttribute(name = "opened")
    public Date getOpened() {
        return opened;
    }

    @XmlElementWrapper(name = "rows")
    @XmlElement(name = "row")
    public List<ChequeRow> getRows() {
        return rows;
    }
    
    @XmlAttribute(name = "total")
    public BigDecimal getTotal(){
        BigDecimal total = BigDecimal.ZERO;
        for(ChequeRow row : rows){
            total = total.add(row.getTotal());
        }
        
        return total.setScale(2);
    }
}