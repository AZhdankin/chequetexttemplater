package komandor.texttemplater.model;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
public class User {
    private String id;
    private String name;

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @XmlAttribute(name="id")
    public String getId() {
        return id;
    }

    @XmlAttribute(name="name")
    public String getName() {
        return name;
    }
}
