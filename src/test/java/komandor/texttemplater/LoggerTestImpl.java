package komandor.texttemplater;

public class LoggerTestImpl implements Logger {

    public void info(Object message) {
        System.out.println(message);
    }

    public void debug(Object message) {
        System.out.println(message);
    }

    public void error(Object message) {
        System.out.println(message);
    }
    
}
