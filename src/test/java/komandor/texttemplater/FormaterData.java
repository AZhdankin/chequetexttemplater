package komandor.texttemplater;

import javax.xml.bind.annotation.XmlRootElement;
import komandor.texttemplater.model.Cheque;
import komandor.texttemplater.model.Pos;
import komandor.texttemplater.model.User;

/**
 *
 * @author Andrey V Zhdankin
 */
@XmlRootElement(name = "data")
public class FormaterData {
    public Cheque cheque;
    public User user;
    public Pos pos;

    public FormaterData() {
    }

    public FormaterData(Cheque cheque, User user, Pos pos) {
        this.cheque = cheque;
        this.user = user;
        this.pos = pos;
    }
}