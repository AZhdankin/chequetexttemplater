package komandor.texttemplater;

import java.io.File;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.List;
import javax.xml.transform.TransformerException;
import komandor.texttemplater.model.Article;
import komandor.texttemplater.model.Cheque;
import komandor.texttemplater.model.ChequeRow;
import komandor.texttemplater.model.Pos;
import komandor.texttemplater.model.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jesus
 */
public class formaterTest {
    
    public formaterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() throws TransformerException {
        Cheque cheque = new Cheque(233456);
        
        {
            ChequeRow row = new ChequeRow(new Article("Товар 1 очень вкусный товар", new BigDecimal("1.23")), new BigDecimal("2.333"));
            row.inc();
            cheque.addRow(row);
        }
        
        {
            ChequeRow row = new ChequeRow(new Article("Товар 2", new BigDecimal("20")), BigDecimal.ONE);
            cheque.addRow(row);
        }
        
        {
            ChequeRow row = new ChequeRow(new Article("Товар 3", new BigDecimal("4")), BigDecimal.TEN);
            row.dec();
            cheque.addRow(row);
        }
        
        cheque.close();
        
        User user = new User("AA11", "Кассир 1");
        Pos pos = new Pos(22, "К13", "К1301");
        
        FormaterData data = new FormaterData(cheque, user, pos);
        
        Logger logger = new LoggerTestImpl();
        Formatter<Command> formater = new TestFormatter(logger);
        List<Command> cmds = formater.render(new File("src/main/resources/cheque.xsl"), 29, data);
        
        for(Command cmd : cmds){
            System.out.println(cmd);
        }
        
    }
    
    public void log(byte[] bytes){
        System.out.println(new String(bytes, Charset.defaultCharset()));
    }
}