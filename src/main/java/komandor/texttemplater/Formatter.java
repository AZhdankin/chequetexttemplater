package komandor.texttemplater;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXB;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import komandor.texttemplater.data.EmptyData;
import komandor.texttemplater.tags.Template;
import komandor.texttemplater.tags.fiscal.FiscalBold;
import komandor.texttemplater.tags.fiscal.FiscalCloseDocument;
import komandor.texttemplater.tags.fiscal.FiscalCutPaper;
import komandor.texttemplater.tags.fiscal.FiscalDiscount;
import komandor.texttemplater.tags.fiscal.FiscalFeedPaper;
import komandor.texttemplater.tags.fiscal.FiscalOpenCashbox;
import komandor.texttemplater.tags.fiscal.FiscalOpenDocument;
import komandor.texttemplater.tags.fiscal.FiscalPay;
import komandor.texttemplater.tags.fiscal.FiscalRepeatLastDocument;
import komandor.texttemplater.tags.fiscal.FiscalReturn;
import komandor.texttemplater.tags.fiscal.FiscalSale;
import komandor.texttemplater.tags.fiscal.FiscalShiftReport;
import komandor.texttemplater.tags.fiscal.FiscalSubTotal;
import komandor.texttemplater.tags.fiscal.FiscalXReport;
import komandor.texttemplater.tags.fiscal.FiscalZReport;
import komandor.texttemplater.tags.result.Result;

public abstract class Formatter<Output> {
    private final Logger logger;

    public Formatter(Logger logger) {
        this.logger = logger;
    }
    
    /**
     * Трансформация шаблона при помощи XSL
     * @param exportedCheque
     * @param params
     * @return
     * @throws TransformerException 
     */
    private String xsl2xml(String dataXML, File template, Map<String, Object> params) throws TransformerException{
        //трансформер для xsl-преобразования
        TransformerFactory tFactory = TransformerFactory.newInstance();
        javax.xml.transform.Transformer xmlTransformer = tFactory.newTransformer(new StreamSource(template));
            
        if(params != null){
            for (String key : params.keySet())
                xmlTransformer.setParameter(key, params.get(key));
        }
        
        StringWriter outputWriter = new StringWriter();
        xmlTransformer.transform(new StreamSource(new StringReader(dataXML)), new StreamResult(outputWriter));
        return outputWriter.getBuffer().toString();
    }
    
    public <Input> List<Output> render(File template, int paperWidth, Input input) throws TransformerException {
        return this.render(template, paperWidth, input, new HashMap());
    }

    public <Input> List<Output> render(File template, int paperWidth, Input input, Map<String, Object> params) throws TransformerException {
        params.put("paperWidth", paperWidth);
            
        StringWriter dataXmlWriter = new StringWriter();;
        if(input == null){
            JAXB.marshal(new EmptyData(), dataXmlWriter);
        } else {
            JAXB.marshal(input, dataXmlWriter);
        }
        
        String dataXML = dataXmlWriter.toString();
        logger.debug(dataXML);

        String templatedXML = xsl2xml(dataXML, template, params);
        logger.debug(templatedXML);
        
        return parseTemplate(paperWidth, templatedXML);
    }
    
    /**
     * По XML-представлению документа возвращает команды для ФР.
     * @param width
     * @param templatedXML
     * @return 
     */
    private List<Output> parseTemplate(int width, String templatedXML){
        //парсинг получившегося документа
        Template template = JAXB.unmarshal(new StringReader(templatedXML), Template.class);
        template.setWidth(width);
        
        //запуск рендеринга шаблона
        Result result = template.render();
        List<Output> commands = new ArrayList();
        
        //прообразование в команды фискальника
        for (Object obj : result) {
            try {
                Method method = this.getClass().getMethod("transform", new Class[]{ List.class, obj.getClass() });
                method.invoke(this, commands, obj);
            } catch (Exception ex) {
                logger.error("Не найден метод форматирования тега в команду: " + obj.getClass().getSimpleName());
            }
        }
        
        return commands;
    }
    
    /* Методы трансформации команд, определяемые в наследуемых классах каждого фискального регистратора */
    /* ---- должны иметь модификатор public!!! иначе из суперкласса не могут быть вызваны через рефлексию ----- */
    
    public abstract void transform(List<Output> result, FiscalOpenDocument openDocument);
    public abstract void transform(List<Output> result, FiscalCloseDocument closeDocument);
    
    public abstract void transform(List<Output> result, FiscalOpenCashbox openCashbox);
    
    public abstract void transform(List<Output> result, FiscalSale sale);
    public abstract void transform(List<Output> result, FiscalReturn fiscalReturn);
    
    public abstract void transform(List<Output> result, FiscalPay pay);
    public abstract void transform(List<Output> result, FiscalDiscount discount);
    public abstract void transform(List<Output> result, FiscalSubTotal subTotal);
    
    public abstract void transform(List<Output> result, FiscalFeedPaper feedPaper);
    public abstract void transform(List<Output> result, FiscalCutPaper cutPaper);
    
    public abstract void transform(List<Output> result, FiscalXReport xReport);
    public abstract void transform(List<Output> result, FiscalZReport zReport);
    public abstract void transform(List<Output> result, FiscalRepeatLastDocument repeatLastDocument);
    public abstract void transform(List<Output> result, FiscalShiftReport shiftReport);
    
    public abstract void transform(List<Output> result, FiscalBold bold);
    public abstract void transform(List<Output> result, String string);
}
