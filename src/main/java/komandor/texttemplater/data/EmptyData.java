package komandor.texttemplater.data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andrey V Zhdankin
 */
@XmlRootElement(name="data")
public class EmptyData {
    
    //просто фиктивный аттрибут для того чтобы была какаято xml-ка
    @XmlAttribute
    public boolean fictive = true;
}
