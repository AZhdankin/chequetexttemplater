package komandor.texttemplater;

public interface Logger {
    void info(Object message);
    void debug(Object message);
    void error(Object message);
}
