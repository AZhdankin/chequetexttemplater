package komandor.texttemplater.tags;

import komandor.texttemplater.tags.result.Result;
import komandor.texttemplater.tags.fiscal.FiscalSale;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import komandor.texttemplater.tags.fiscal.FiscalCloseDocument;
import komandor.texttemplater.tags.fiscal.FiscalCutPaper;
import komandor.texttemplater.tags.fiscal.FiscalDiscount;
import komandor.texttemplater.tags.fiscal.FiscalFeedPaper;
import komandor.texttemplater.tags.fiscal.FiscalOpenCashbox;
import komandor.texttemplater.tags.fiscal.FiscalOpenDocument;
import komandor.texttemplater.tags.fiscal.FiscalPay;
import komandor.texttemplater.tags.fiscal.FiscalRepeatLastDocument;
import komandor.texttemplater.tags.fiscal.FiscalReturn;
import komandor.texttemplater.tags.fiscal.FiscalShiftReport;
import komandor.texttemplater.tags.fiscal.FiscalSubTotal;
import komandor.texttemplater.tags.fiscal.FiscalXReport;
import komandor.texttemplater.tags.fiscal.FiscalZReport;
import komandor.texttemplater.tags.nodes.Node;
import komandor.texttemplater.tags.nodes.PrintNode;
import komandor.texttemplater.tags.table.Table;

/**
 *
 * @author Andrey V Zhdankin
 */
public abstract class Container extends PrintNode {
    private List<Node> nodes;

    public Container() {
    }

    public Container(List<Node> nodes) {
        this.nodes = nodes;
    }
    
    @XmlElements({
        //string tags:
        @XmlElement(name="table",  type=Table.class),
        @XmlElement(name="hr",     type=Hr.class),
        @XmlElement(name="text",   type=Text.class),
        
        //fiscal tags:
        @XmlElement(name="fiscalOpenDocument",  type=FiscalOpenDocument.class),
        @XmlElement(name="fiscalCloseDocument", type=FiscalCloseDocument.class),
        @XmlElement(name="fiscalSale",          type=FiscalSale.class),
        @XmlElement(name="fiscalReturn",        type=FiscalReturn.class),
        @XmlElement(name="fiscalPay",           type=FiscalPay.class),
        @XmlElement(name="fiscalOpenCashbox",   type=FiscalOpenCashbox.class),
        @XmlElement(name="fiscalCutPaper",      type=FiscalCutPaper.class),
        @XmlElement(name="fiscalFeedPaper",     type=FiscalFeedPaper.class),
        @XmlElement(name="fiscalXReport",       type=FiscalXReport.class),
        @XmlElement(name="fiscalZReport",       type=FiscalZReport.class),
        @XmlElement(name="fiscalRepeatLastDocument", type=FiscalRepeatLastDocument.class),
        @XmlElement(name="fiscalShiftReport",   type=FiscalShiftReport.class),
        @XmlElement(name="fiscalDiscount",      type=FiscalDiscount.class),
        @XmlElement(name="fiscalSubTotal",      type=FiscalSubTotal.class)
    })
    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    @XmlAttribute
    @Override
    public int getWidth() {
        return super.getWidth();
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        
        if(this.getNodes() != null){
            for (Node node : this.getNodes()) {
                if (node instanceof PrintNode) {
                    PrintNode printNode = (PrintNode) node;
                    printNode.setWidth(width);
                }
            }
        }
    }

    @Override
    public Result render() {
        Result res = new Result();
        if(this.getNodes() != null){
            for (Node node : this.getNodes()) {
                res.addAll(node.render());
            }
        }
        
        return res;
    }
}
