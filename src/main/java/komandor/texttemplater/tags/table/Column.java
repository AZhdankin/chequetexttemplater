package komandor.texttemplater.tags.table;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Column {
    private boolean resize = false;

    public Column() {
    }
    
    public Column(boolean resize) {
        this.resize = resize;
    }

    @XmlAttribute
    public boolean isResize() {
        return resize;
    }

    public void setResize(boolean resize) {
        this.resize = resize;
    }
}
