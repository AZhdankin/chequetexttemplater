package komandor.texttemplater.tags.table;

import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import komandor.texttemplater.tags.Container;
import komandor.texttemplater.tags.nodes.Node;
import komandor.texttemplater.tags.result.Result;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Cell extends Container {
    private boolean resize = false;
    private int colspan = 1;
    private FictiveCell rightFictCell = null;
    
    public Cell() {
        
    }

    public Cell(List<Node> nodes) {
        super(nodes);
    }

    public Cell(Node... nodes) {
        this(Arrays.asList(nodes));
    }

    public boolean isResize() {
        return resize;
    }

    public void setResize(boolean resize) {
        this.resize = resize;
    }
    
    @XmlAttribute
    @Override
    public int getWidth() {
        //раширяемая вправо клетка не может объективно сказать о своих размерах
        if(rightFictCell != null)
            return 0;
        
        return super.render().getWidth();
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
    }
    
    @XmlAttribute
    public int getColspan() {
        return colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    @XmlTransient
    public FictiveCell getRightFictCell() {
        return rightFictCell;
    }

    public void setRightFictCell(FictiveCell rightFictCell) {
        this.rightFictCell = rightFictCell;
    }

    @Override
    public Result render() {
        //если есть фиктивная ячейка справа, то размер текущей ячеки для рендеринга складывается с размером ячейки справа
        if(rightFictCell != null)
            this.setWidth(super.getWidth() + rightFictCell.getSettedWidth());
        
        return super.render();
    }
}
