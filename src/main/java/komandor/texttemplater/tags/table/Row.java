package komandor.texttemplater.tags.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import komandor.texttemplater.tags.nodes.PrintNode;
import komandor.texttemplater.tags.result.Result;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Row extends PrintNode {
    private List<Cell> cells;
    
    @XmlElement(name="cell")
    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }
    
    public int cellCount(){
        return cells.size();
    }
    
    public void changeColumnsWidth(List<Column> columns, Map<Column, Integer> colWidth){
        //применяем новые размеры к ячейкам таблицы и рендерим строки
        for(Cell cell : cells){
            int colIndex = cells.indexOf(cell);
            Column col = columns.get(colIndex);
            int newWidth = colWidth.get(col);
            cell.setWidth(newWidth);
        }
    }
    
    /**
     * Рендеринг строки шаблона
     * @param ctx
     * @return 
     */
    @Override
    public Result render() {
        //рендерятся ячейки и вычисляется максимальная высота ячейки
        int maxHeight = 0;
        Map<Cell, Result> results = new HashMap();
        for(Cell cell : cells){
            Result tmp = cell.render();
            results.put(cell, tmp);
            maxHeight = Math.max(maxHeight, tmp.getHeight());
        }
        
        //собирается отрендеренная строка и выравнивается высота каждой ячейки до максимальной
        List<StringBuilder> strings = new ArrayList();
        for(int i=0; i<maxHeight; i++){
            StringBuilder string = new StringBuilder();
            for(Cell cell : cells){
                String renderedText;
                if (cell instanceof FictiveCell) { //если ячейка фиктивная, то вместо нее должна быть пустая строка
                    renderedText = "";
                } else {
                    Result tmp = results.get(cell);

                    if(tmp.getHeight() >= i)    
                        renderedText = tmp.getString(i);
                    else
                        renderedText = StringUtils.repeat(" ", cell.getWidth());
                }
                string.append(renderedText);
            }
            strings.add(string);
        }
        
        //преобразование строк в набор результата
        Result result = new Result();
        for(StringBuilder str : strings){
            result.add(str.toString());
        }
        
        //добавляем после строки, содержащиеся в ней фискальные команды в результат
        for(Cell cell : cells){
            result.addAll(results.get(cell).getFiscalCommands());
        }
        
        return result;
    }

    /**
     * Выравнивает строку фиктивными ячейками.
     */
    public void createFictiveCells() {
        for(int i=0; i<this.getCells().size(); i++){
            Cell currentCell = this.getCells().get(i);
            
            int colspan = currentCell.getColspan();
            
            if(colspan > 1){
                FictiveCell cell = new FictiveCell();
                cell.setColspan(colspan-1);
                
                currentCell.setRightFictCell(cell);
                this.getCells().add(i + 1, cell);
            }
        }
    }
}
