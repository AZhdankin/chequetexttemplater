package komandor.texttemplater.tags.table;

import komandor.texttemplater.tags.result.Result;

/**
 * Класс фиктивной ячейки, которая создана через расширяемую ячейку
 * @author Andrey V Zhdankin
 */
public class FictiveCell extends Cell {
    private int settedWidth = 0;

    @Override
    public void setWidth(int width) {
        this.settedWidth = width;
    }

    @Override
    public int getWidth() {
        return 0;
    }
    
    public int getSettedWidth() {
        //сама ячейка должна всем говорить что ее ширина нулевая, чтобы не влиять на размер столбца.
        //а тем кто знает что она фиктивная, она сообщает свой размер 
        //как сумму установленной ей ширины и ширины соседней фиктивной ячейки
        FictiveCell rightFictCell = this.getRightFictCell();
        if(rightFictCell != null)
            return rightFictCell.getSettedWidth() + settedWidth;
        
        return settedWidth;
    }

    @Override
    public Result render() {
        return new Result();
    }
}
