package komandor.texttemplater.tags.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import komandor.texttemplater.tags.Container;
import komandor.texttemplater.tags.result.Result;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Table extends Container {
    private List<Column> columns;
    private List<Row> rows;

    public Table() {
    }

    @XmlElement(name="column")
    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    @XmlElement(name="row")
    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public Result render(){
        //инициализация расчета столбцов
        Map<Column, Integer> colWidth = new HashMap();
        
        for(Row row : rows){
            row.createFictiveCells();//разъединение объединенных ячеек в строке
            
            //вычисление максимальных значений длин столбцов
            for(Cell cell : row.getCells()){
                Column col = getColumns().get(row.getCells().indexOf(cell));
                Integer currentWidth = cell.getWidth();

                if(!colWidth.containsKey(col)){ //если нет значения размера стобца
                    colWidth.put(col, currentWidth);
                    continue;
                }

                int newWidth = Math.max(colWidth.get(col), currentWidth);
                colWidth.put(col, newWidth);
            }
        }
        
        //подсчет ширины таблицы
        int totalWidth = 0;
        for(int width : colWidth.values()){
            totalWidth += width;
        }
        
        //необходимое абсолютное изменение
        int kAbs = this.getWidth() - totalWidth;
        
        //составляем список изменяемых столбцов
        List<Column> changeCols = new ArrayList();
        int oldWidth = 0; //старая сумма длин
        for(Column col : this.getColumns()){
            if(col.isResize()){
                changeCols.add(col);
                oldWidth += colWidth.get(col);
            }
        }
        
        //если колонки уменьшаются, то может не хватить изменяемых столбцов на уменьшение
        if(changeCols.isEmpty() || (kAbs < 0 && oldWidth < Math.abs(kAbs))){
            //добавляем по очереди колонки
            for(Column col : this.getColumns()){
                if(changeCols.contains(col))
                    continue;

                changeCols.add(col);
                oldWidth += colWidth.get(col);

                //останавливаемся если теперь будет хватать
                if(oldWidth >= Math.abs(kAbs))
                    break;
            }
        }
        
        //коэфициент изменения изменяемых столбцов
        float k = (float) (oldWidth + kAbs) / oldWidth;
        
        //если изменяемых столбцов нет, то изменяем первый
        if(changeCols.isEmpty())
            changeCols.add(this.getColumns().get(0));
        
        //применяем коэффициент к изменяемым столбцам
        for(Column col : changeCols){
            int currentWidth = colWidth.get(col);
            int newWidth = (int) Math.ceil(currentWidth * k);
            colWidth.put(col, newWidth);
        }
        
        //подсчет новой суммы длин
        int newWidth = 0;
        for(Column col : changeCols){
            newWidth += colWidth.get(col);
        }
        
        //коррекция погрешности, если нужно
        int diff = oldWidth - newWidth + kAbs; //погрешность
        if (diff != 0){
            //гасим погрешность за счет первого изменяемого столбца
            Column firstCol = changeCols.get(0);
            int firstColumnWidth = colWidth.get(firstCol) + diff;
            colWidth.put(firstCol, firstColumnWidth);
        }
        
        //делегируем строкам применение изменения ширины ячеек и рендерим строки
        Result result = new Result();
        for(Row row : this.getRows()){
            row.changeColumnsWidth(getColumns(), colWidth);
            result.addAll(row.render());
        }
        return result;
    }
}
