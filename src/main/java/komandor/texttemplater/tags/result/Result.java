package komandor.texttemplater.tags.result;

import java.util.ArrayList;
import java.util.Arrays;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Result extends ArrayList {

    public Result() {
        super();
    }
    
    public Result(Object... obj) {
        super();
        this.addAll(Arrays.asList(obj));
    }
    
    public int getWidth(){
        int maxWidth = 0;
        for(Object obj : this){
            if(obj instanceof String){
                String str = (String) obj;
                if (maxWidth < str.length())
                    maxWidth = str.length();
            }
        }
        
        return maxWidth;
    }
    
    public int getHeight(){
        int height = 0;
        for(Object obj : this){
            if(obj instanceof String){
                height++;
            }
        }
        
        return height;
    }

    public String getString(int index) {
        int height = 0;
        for(Object obj : this){
            if(obj instanceof String){
                String str = (String) obj;
                if(height == index)
                    return str;
                height++;
            }
        }
        
        return null;
    }
    
    public Result getFiscalCommands(){
        Result result = new Result();
        
        for(Object obj : this){
            if(obj instanceof FiscalNode){
                result.add(obj);
            }
        }
        
        return result;
    }
}
