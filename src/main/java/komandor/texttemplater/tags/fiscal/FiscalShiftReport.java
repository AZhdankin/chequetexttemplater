package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalShiftReport extends FiscalNode {
    private int shiftNumber;

    public FiscalShiftReport() {
    }

    public FiscalShiftReport(int shiftNumber) {
        this.shiftNumber = shiftNumber;
    }

    @XmlAttribute
    public int getShiftNumber() {
        return shiftNumber;
    }

    public void setShiftNumber(int shiftNumber) {
        this.shiftNumber = shiftNumber;
    }
}
