package komandor.texttemplater.tags.fiscal;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalDiscount extends FiscalNode {
    private String name = "";
    private boolean percent = false;
    private BigDecimal amount = BigDecimal.ZERO;

    public FiscalDiscount() {
    }
    
    public FiscalDiscount(String name, boolean percent, BigDecimal amount) {
        this.name = name;
        this.percent = percent;
        this.amount = amount;
    }

    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlAttribute
    public boolean isPercent() {
        return percent;
    }

    public void setPercent(boolean percent) {
        this.percent = percent;
    }

    @XmlAttribute
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    
}
