package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalFeedPaper extends FiscalNode {
    private int rows = 7;

    public FiscalFeedPaper() {
    }

    public FiscalFeedPaper(int rows) {
        this.rows = rows;
    }

    @XmlAttribute
    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }
}
