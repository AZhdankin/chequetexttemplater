package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalOpenDocument extends FiscalNode {
    private long id;
    private String user;
    private String type;

    public FiscalOpenDocument() {
    }

    public FiscalOpenDocument(long id, String user) {
        this.id = id;
        this.user = user;
    }

    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @XmlAttribute
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
