package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalCutPaper extends FiscalNode {
    private boolean full = true;

    public FiscalCutPaper() {
    }
    
    public FiscalCutPaper(boolean full) {
        this.full = full;
    }

    @XmlAttribute
    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }
}
