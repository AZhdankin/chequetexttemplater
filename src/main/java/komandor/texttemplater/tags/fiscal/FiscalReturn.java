package komandor.texttemplater.tags.fiscal;

import komandor.texttemplater.tags.nodes.FiscalNode;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalReturn extends FiscalNode {
    private String text;
    private BigDecimal price;
    private BigDecimal count;

    public FiscalReturn() {
    }

    public FiscalReturn(String text, BigDecimal price, BigDecimal count) {
        this.text = text;
        this.price = price;
        this.count = count;
    }

    @XmlAttribute
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    

    @XmlAttribute
    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    @XmlAttribute
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
