package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalCloseDocument extends FiscalNode {
    private boolean cut = false;

    public FiscalCloseDocument() {
    }

    public FiscalCloseDocument(boolean cut) {
        this.cut = cut;
    }

    @XmlAttribute
    public boolean isCut() {
        return cut;
    }

    public void setCut(boolean cut) {
        this.cut = cut;
    }
}
