package komandor.texttemplater.tags.fiscal;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalPay extends FiscalNode {
    private BigDecimal cash     = BigDecimal.ZERO;
    private BigDecimal cashless = BigDecimal.ZERO;
    private BigDecimal kopilka  = BigDecimal.ZERO;

    public FiscalPay() {
    }

    public FiscalPay(BigDecimal cash, BigDecimal cashless, BigDecimal kopilka) {
        this.cash = cash;
        this.cashless = cashless;
        this.kopilka = kopilka;
    }
    
    @XmlAttribute
    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    @XmlAttribute
    public BigDecimal getCashless() {
        return cashless;
    }

    public void setCashless(BigDecimal cashless) {
        this.cashless = cashless;
    }

    @XmlAttribute
    public BigDecimal getKopilka() {
        return kopilka;
    }

    public void setKopilka(BigDecimal kopilka) {
        this.kopilka = kopilka;
    }
}
