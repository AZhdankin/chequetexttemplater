package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalBold extends FiscalNode {
    private boolean bold = false;

    public FiscalBold() {
    }

    public FiscalBold(boolean bold) {
        this.bold = bold;
    }

    @XmlAttribute
    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }
}
