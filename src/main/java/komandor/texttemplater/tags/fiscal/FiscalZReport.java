package komandor.texttemplater.tags.fiscal;

import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.nodes.FiscalNode;

/**
 *
 * @author Andrey V Zhdankin
 */
public class FiscalZReport extends FiscalNode {
    private String userName = "";

    public FiscalZReport() {
    }

    public FiscalZReport(String userName) {
        this.userName = userName;
    }

    @XmlAttribute
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
