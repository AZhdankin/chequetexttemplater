package komandor.texttemplater.tags.nodes;

import komandor.texttemplater.tags.result.Result;

/**
 * Базовый интерфейс для тегов.
 * @author Andrey V Zhdankin
 */
public interface Node {
    
    public Result render();
}
