package komandor.texttemplater.tags.nodes;

import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrey V Zhdankin
 */
@XmlTransient
public abstract class PrintNode implements Node {
    private int width = 0;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
