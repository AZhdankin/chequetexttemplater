package komandor.texttemplater.tags.nodes;

import komandor.texttemplater.tags.result.Result;

/**
 *
 * @author Andrey V Zhdankin
 */
public abstract class FiscalNode implements Node {
    
    public Result render(){
        return new Result(this);
    }
}
