package komandor.texttemplater.tags;

import komandor.texttemplater.tags.nodes.PrintNode;
import komandor.texttemplater.tags.result.Result;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import komandor.texttemplater.tags.fiscal.FiscalBold;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Text extends PrintNode {
    private String align;
    private String value;
    private boolean wordWrap = false;
    private boolean bold = false;

    @XmlAttribute
    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    @XmlAttribute
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    @XmlAttribute
    public int getWidth() {
        if(super.getWidth() != 0)
            return super.getWidth();
        
        return this.render().getWidth();
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
    }

    @XmlAttribute(name="word-wrap")
    public boolean isWordWrap() {
        return wordWrap;
    }

    public void setWordWrap(boolean wordWrap) {
        this.wordWrap = wordWrap;
    }

    @XmlAttribute
    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public Result render(){
        //если ширина текста не указана, то текст не форматируется.
        if (super.getWidth() == 0)
            return new Result(this.getValue());
        
        List<StringBuilder> strings = new ArrayList(); 
        StringBuilder currentString = new StringBuilder(this.getWidth());
        
        //разбиваем весь текст на слова по пробелам
        String[] words = StringUtils.splitByCharacterTypeCamelCase(this.getValue());
        
        //перебираем слова
        for (int i=0; i<words.length; i++){
            String word = words[i];
            
            //остаток места для слов в строке
            int ostatok = this.getWidth() - currentString.length();
            
            //если в строке осталось места меньше чем длина слова
            if (ostatok <= word.length()){
                //если перенос по словам запрещен, то дописываем сколько можем
                if (!this.isWordWrap()){
                    currentString.append(StringUtils.left(word, ostatok));
                    break;
                }
                
                //строку добавляем в результат и создаем новую строку
                strings.add(currentString);
                currentString = new StringBuilder(this.getWidth());
                currentString.append(word);
                continue;
            }
            
            //вставим пробел перед словом, если это не первое слово
            currentString.append(word);
        }
        
        //добавляем последнюю строку в результат
        strings.add(currentString);
        
        //получаем результирующий набор строк
        Result result = new Result();
        
        if(this.isBold())
            result.add(new FiscalBold(true));
        
        for (StringBuilder stringBuilder : strings){
            String str = stringBuilder.toString();
            str = setupAlign(str, this.getAlign());
            result.add(str);
        }
        
        if(this.isBold())
            result.add(new FiscalBold(false));
        
        return result;
    }
    
    /**
     * Выравнивание строк.
     * @param str = строка для выравнивания
     * @param align = left, center, right
     * @param strSize = paperWidth
     * @return выровненная строка
     */
    private String setupAlign(String str, String align) {
        if("left".equals(align))
            return StringUtils.rightPad(str, this.getWidth());
        
        if("center".equals(align))
            return StringUtils.center(str, this.getWidth());
        
        if("right".equals(align))
            return StringUtils.leftPad(str, this.getWidth());
        
        return str;
    }
}
