package komandor.texttemplater.tags;

import komandor.texttemplater.tags.nodes.PrintNode;
import komandor.texttemplater.tags.result.Result;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Andrey V Zhdankin
 */
public class Hr extends PrintNode {
    
    @Override
    public Result render() {
        return new Result(StringUtils.repeat("-", this.getWidth()));
    }
}
