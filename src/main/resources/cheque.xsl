<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="data">
        <xsl:param name="paperWidth"/>
        <template>
            <fiscalOpenDocument />
            <table>
                <column resize="true"/>
                <column resize="true"/>
                <row>
                    <cell><text word-wrap="false" align="left" value="{user/@name}"/></cell>
                    <cell><text word-wrap="false" align="right" value="{pos/@pointId}/{cheque/@id}"/></cell>
                </row>
            </table>
            <xsl:choose>
                <xsl:when test="$paperWidth &lt; 30">
                    <table>
                        <column resize="true" />
                        <column resize="true" />
                        <column resize="true" />
                        <xsl:for-each select="cheque/rows/row">
                            <row>
                                <cell colspan="3"><text align="left" word-wrap="true" value="{position()}.{article/@name}"/></cell>
                            </row>
                            <row>
                                <cell><text bold="true" word-wrap="false" align="right" value="{article/@price}"/>
                                </cell>
                                <cell><text word-wrap="false" align="left" value=" x {@count}"/></cell>
                                <cell>
                                    <text word-wrap="false" align="right" value=" ={@total}"/>
                                    <fiscalSale price="{article/@price}" count="{@count}" />
                                </cell>
                            </row>
                        </xsl:for-each>
                    </table>
                </xsl:when>
                <xsl:otherwise>
                    <table>
                        <column resize="true" />
                        <column resize="false" />
                        <column resize="false" />
                        <column resize="false" />
                        <xsl:for-each select="cheque/rows/row">
                            <row>
                                <cell><text align="left" word-wrap="false" value="{position()}.{article/@name}"/></cell>
                                <cell><text align="right" value="{article/@price} x {@count}"/></cell>
                                <cell><text align="left" value=" =" width="2"/></cell>
                                <cell>
                                    <text align="right" value="{@total}"/>
                                    <fiscalSale price="{article/@price}" count="{@count}" />
                                </cell>
                            </row>
                        </xsl:for-each>
                    </table>
                </xsl:otherwise>
            </xsl:choose>
            <table>
                <column resize="false"/>
                <column resize="false"/>
                <row>
                    <cell><text align="left" word-wrap="false" value="Итого по чеку"/></cell>
                    <cell><text align="right" word-wrap="false" value=" ={cheque/@total}"/></cell>
                </row>
            </table>
            <hr />
            <text align="center" word-wrap="true" bold="true" value="Служба поддержки 8 800 2000 500 Служба поддержки 8 800 2000 500 Служба поддержки 8 800 2000 500"/>
            <fiscalPay cash="{cheque/@total}" />
            <fiscalCloseDocument />
        </template>
    </xsl:template>
</xsl:stylesheet>
